import argparse
import model
import s3fs
import os
import csv
import psycopg2
import pyarrow.parquet as pq
import pandas as pd
import sys
import numpy as np
from collections import Counter
from itertools import combinations, groupby

parser = argparse.ArgumentParser()
parser.add_argument('-s3Path', help='Source data location(Ceph bucket subfolder)', default='None')
parser.add_argument('-s3endpointUrl', help='Ceph backend endpoint URL', default='None')
parser.add_argument('-s3objectStoreLocation', help='Ceph Store (Bucket) Name', default='DH-DEV-INSIGHTS')
parser.add_argument('-s3accessKey', help='Ceph Store Access Key', default='None')
parser.add_argument('-s3secretKey', help='Ceph Store Secret key', default='None')
parser.add_argument('-destination', help='Location to store results', default='/associative_rule_learning/rules.csv')
args=parser.parse_args()

#source = '2018-07-01/rule_data'

##ceph_endpoint = os.environ.get('CEPH_S3_ENDPOINT')
##print(ceph_endpoint)
clientKwargs = {'endpoint_url': args.s3endpointUrl}
s3 = s3fs.S3FileSystem(secret=args.s3secretKey, key=args.s3accessKey, client_kwargs=clientKwargs)


#*******************model start******************
# read dataset from s3_bucket location 

df_rules = pq.ParquetDataset(os.path.join(args.s3objectStoreLocation, args.s3Path), filesystem=s3).read_pandas().to_pandas()
print("Data collected from ParquetDataset")

pd_flat = model.data_processing(df_rules)

# %%time
rules = model.association_rules(pd_flat, 0.01)

print("Rules generation is complete.")

source_file = "rules.csv"

print("source_file:" + source_file)
rules.to_csv(source_file)

#Store results in Ceph backend

destination = args.s3objectStoreLocation + args.destination
print("destination:" + destination)

limit = 100
n = 0
with s3.open(destination, "wb") as fw:
	with open("rules.csv", "r") as fr:
		reader = csv.reader(fr, delimiter=',')
		for row in reader:
			n += 1
			print(row)	
			fw.write(str(row).encode('utf-8'))	
			if n == limit:
				break
print("done!")
# Upload file to the destination folder in the Ceph backend



