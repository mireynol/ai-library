import os
import requests
import json
import time
import boto3
import argparse

def create_session_and_resource(s3accessKey, s3secretKey, s3endpointUrl):
    try:
        #Create S3 session to access Ceph backend
        print("Creating session to access Ceph backend..!")
        session = boto3.Session(
            aws_access_key_id=s3accessKey,
            aws_secret_access_key=s3secretKey
        )
        #Get an S3 resource
        s3 = session.resource('s3',
      	         endpoint_url=s3endpointUrl,
	         verify=False)
    except Exception as ex:
        raise Exception('Error while creating session to access object storage.' + str(ex))
    return s3

def download_data(s3, s3objectStoreLocation, sourcefile, destinationfile):
    try:
        # Download file from ceph backend 
        s3.meta.client.download_file(s3objectStoreLocation, sourcefile, destinationfile)
        print("Data download from Ceph backend complete..!")
    except Exception as ex:
        raise Exception('Error while reading data from Ceph backend.' + str(ex))

    #Verify if the file exists in the current directory after downloading it from the
    #Ceph backend.    

    if not os.path.exists(destinationfile):
       print('File does not exist in the specified location!')
    else:
       print("Verified that object has been downloaded to destination")

def upload_data(s3, s3objectStoreLocation, sourcefile, destinationfile):
    try:
        # Upload file to the destination folder in the Ceph backend
        s3.meta.client.upload_file(sourcefile, s3objectStoreLocation, destinationfile)
        print("Data upload to Ceph backend complete..!")
    except Exception as ex:
        raise Exception('Error while writing file to the object storage.' + str(ex))

    try:
        # Verify if the file exists in the Ceph backend
        s3.Object(s3objectStoreLocation, destinationfile).load()
        print("Verified that object exists in Ceph backend")
    except Exception as e:
        raise Exception('Error while checking if data exists in Ceph backend.' + str(e))

parser=argparse.ArgumentParser()
parser.add_argument('-source', help='Source data location (Web location or local file)', default='None')
parser.add_argument('-destination', help='Destination data location (Ceph bucket subfolder)', default='None')
parser.add_argument('-s3endpointUrl', help='Ceph backend endpoint URL', default='None')
parser.add_argument('-s3objectStoreLocation', help='Ceph Store (Bucket) Name', default='DH-DEV-DATA')
parser.add_argument('-s3accessKey', help='Ceph Store Access Key', default='None')
parser.add_argument('-s3secretKey', help='Ceph Store Secret key', default='None')
parser.add_argument('-ops', help='upload or download data', default='None')
args=parser.parse_args()

source = args.source
s3 = create_session_and_resource(args.s3accessKey, args.s3secretKey, args.s3endpointUrl)

if args.ops == "upload":
 #Check if file exists locally, if not download from the web location
 if not (os.path.isfile(source)):
   if source.find('/'):
     cmd = "wget " + source
     os.system(cmd)
     source  = source.rsplit('/', 1)[1]
 upload_data(s3, args.s3objectStoreLocation, source, args.destination)
elif args.ops == "download":
 download_data(s3, args.s3objectStoreLocation, source, args.destination)
else:
 print('Unsupported option for ops. Should be either upload or download!')
