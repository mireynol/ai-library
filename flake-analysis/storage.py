import os
import boto3

def create_session_and_resource(s3accessKey, s3secretKey, s3endpointUrl):
    try:
        #Create S3 session to access Ceph backend
        print("Creating session to access Ceph backend..!")
        session = boto3.Session(
            aws_access_key_id=s3accessKey,
            aws_secret_access_key=s3secretKey
        )
        #Get an S3 resource
        s3 = session.resource('s3',
      	         endpoint_url=s3endpointUrl,
	         verify=False)
    except Exception as ex:
        raise Exception('Error while creating session to access object storage.' + str(ex))
    return s3

def download_data(s3, s3objectStoreLocation, sourcefile, destinationfile):
    try:
        # Download file from ceph backend in to the current working directory under 
        # the same file name as the source
        s3.meta.client.download_file(s3objectStoreLocation, sourcefile, destinationfile)
        print("Data download from Ceph backend complete..!")
    except Exception as ex:
        raise Exception('Error while reading data from Ceph backend.' + str(ex))

    #Verify if the file exists in the current directory after downloading it from the
    #Ceph backend.    

    if not os.path.exists(destinationfile):
       raise ValueError('File does not exist in the specified location!')

def upload_data(s3, s3objectStoreLocation, sourcefile, destinationfile):
    try:
        # Upload file to the destination folder in the Ceph backend
        s3.meta.client.upload_file(sourcefile, s3objectStoreLocation, destinationfile)
        print("Data upload to Ceph backend complete..!")
    except Exception as ex:
        raise Exception('Error while writing file to the object storage.' + str(ex))

    try:
        # Verify if the file exists in the Ceph backend
        s3.Object(s3objectStoreLocation, destinationfile).load()
        print("Verified that object exists in Ceph backend")
    except Exception as e:
        raise Exception('Error while checking if data exists in Ceph backend.' + str(e))

