import datetime
import time
import csv
import io
import os
import boto3
import uuid
import argparse
import subprocess
import storage as ceph

parser=argparse.ArgumentParser()
parser.add_argument('-s3Path', help='Source data location (Ceph bucket subfolder)', default='None')
parser.add_argument('-s3endpointUrl', help='Ceph backend endpoint URL', default='None')
parser.add_argument('-s3objectStoreLocation', help='Ceph Store (Bucket) Name', default='DH-DEV-DATA')
parser.add_argument('-s3accessKey', help='Ceph Store Access Key', default='None')
parser.add_argument('-s3secretKey', help='Ceph Store Secret key', default='None')
args=parser.parse_args()

# Create S3 session to access Ceph backend and get an S3 resource
s3 = ceph.create_session_and_resource(args.s3accessKey, args.s3secretKey, args.s3endpointUrl)

#Extract filename from destination url
urlsplit = args.s3Path.split("/")
filename = urlsplit[-1]
folder = '/'.join(urlsplit[:-1])

# Download file from ceph backend in to the current working directory under 
# the same file name as the source

ceph.download_data(s3, args.s3objectStoreLocation, args.s3Path, filename)

#Execute learn-tests to train the model using the data file
filename = (args.s3Path).split('/')
cmd = "python flake-analysis/bots/learn-tests --dry " + filename[-1]
os.system(cmd)
print("Training complete..!")

cmd = 'find flake-analysis/bots/images/ -name tests-learn*.model -printf "%f"'
model = os.popen(cmd).read()
print("Found model : " + model)

destination = folder + "/" + model
print("Uploading model to: " + destination)

# Upload file to the destination folder in the Ceph backend
ceph.upload_data(s3, args.s3objectStoreLocation, "flake-analysis/bots/images/" + model, destination)

