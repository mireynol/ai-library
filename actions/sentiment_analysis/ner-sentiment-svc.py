# This action takes in text and processes the sentiment along with entity relationships.
# The results are stored in ElasticSearch and Ceph.

import os
import re
import csv
import codecs
import timeit
import pickle
import argparse
import json
import spacy
import requests
import uuid
import boto3
from textblob import TextBlob
from time import gmtime, strftime
from pycorenlp.corenlp import StanfordCoreNLP
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

#### Vader module to get sentiment of a text

analyser = SentimentIntensityAnalyzer()
sent_dict ={'neg':'Negative','pos':'Positive','neu':'Neutral'}
def get_vader_sentiment(text):
    try:
        score = analyser.polarity_scores(text)
        max_score = score['neg']
        max_label = 'neg'
        if score['pos'] > max_score:
            max_score = score['pos']
            max_label = 'pos' 
        if score['neu'] > max_score:
            max_score = score['neu']
            max_label = 'neu' 
        return sent_dict[max_label]
    except Exception:
        return "Null Sentiment"

#### Text blob module to get sentiment from a text		

def get_text_blob_sentiment(text):
    sentiment =''
    try:
        blob = TextBlob(text)
        if blob.sentiment.polarity > 0.2:
            sentiment = 'Positive'
        elif blob.sentiment.polarity < -0.2:
            sentiment = 'Negative'
        else:
            sentiment ='Neutral'
    except Exception:
        sentiment = 'Null Sentiment'
    return sentiment

def neg_sentiment_validator(text):
    sent_dict = {}
    sent_dict['Neutral'] = 0 
    sent_dict['Negative'] = 1 
    sent_dict['Positive'] = 0 
    sent_stanford_nlp = 'Negative'
    sent_dict[get_vader_sentiment(text)] += 1
    sent_dict[get_text_blob_sentiment(text)] += 1
    output_sentiment = 'Neutral'
    if sent_dict['Negative'] > sent_dict['Neutral']:
        output_sentiment = 'Negative'
    if sent_dict['Positive'] > sent_dict['Neutral']:
        output_sentiment = 'Positive'
    
    return output_sentiment

# Make a call to the Stanford NLP server to detect the sentiment
def sentiment_core_nlp(requestId, groupingId, nlp_spacy, nlp, text, owner, created):
    if not isinstance(text, str):
        return "Null Sentiment"
    
    else:
        output = nlp.annotate(
            text,
            properties={
                "outputFormat": "json",
                "annotators": "sentiment"
            }
        )
        try:
            values = []
            # The NLP server returns sentiment and entities detected by sentence
            # Loop through the sentences to convert the results to a JSON array
            for a in output['sentences']:
                sentence = " ".join([t["word"] for t in a["tokens"]])
                
                #Detect entities
                doc = nlp_spacy(sentence)
                entities = doc.ents
                sentiment = a["sentiment"].lower()
                #sentimentValue = a["sentimentValue"]

                #CoreNLP seems to weight more towards negative than neutral.
                #To balance that, other models will be used if negative is detected.
                if (sentiment == "negative"):
                    sentiment = neg_sentiment_validator(sentence).lower()

                entityArray = []
                additionalEntityArray = []
                for entity in doc.ents:
                    if not entity.text.isspace():
                        label = entity.label_.upper()
                        # Add entities that aren't useful to a separate JSON object
                        if label == "LOC" or label == "MONEY" or label == "DATE" or label == "TIME" or label == "QUANTITY" or label == "CARDINAL" or label == "ORDINAL":
                            additionalEntityArray.append({"name": entity.text, "category": label})
                        else:
                            entityArray.append({"name": entity.text, "category": label})

                element = {"requestId": requestId, "groupingId": groupingId, "sentence": sentence, "sentiment": sentiment, "entities": entityArray, "otherEntities": additionalEntityArray, "created": created}

                if owner is not None:
                    element['owner'] = owner

                values.append(element)
            return values
        except (TypeError, Exception) as e:
            print('An error occurred with the sentiment: ' + str(e))
            return []

# Store the results in Ceph and ElasticSearch
def saveCustomerFeedbackAnalysis(s3, s3Bucket, s3Path, esEndpoint, esIndex, esRawDataIndex, groupingId, records, data, dataType, overrideExistingRecords):
    try:
        # First store the results to S3
        jsonData = json.dumps(data)
        s3.meta.client.put_object(Body=json.dumps(records), Bucket=s3Bucket, Key=s3Path + 'analysis/' + groupingId)
        #s3.meta.client.put_object(Body=jsonData, Bucket=s3Bucket, Key=s3Path + 'raw/' + groupingId)

        if overrideExistingRecords:
            # Delete the existing data for this record if it exists
            response = requests.post(esEndpoint + '/' + esIndex + '/_delete_by_query', data=json.dumps({"query": {"match": {"groupingId": groupingId}}}),cert=('access.crt', 'access.key'), verify=False,headers={'content-type': 'application/json'})

        # Second store the results in Elasticsearch. Each sentence gets a separate record stored.
        bulkData = ''
        for record in records:
            bulkData += '{ "index" : { "_index" : "' + esIndex + '", "_type" : "' + dataType + '" } }\n' + json.dumps(record) + '\n'
        #bulkData += '{ "index" : { "_index" : "' + esRawDataIndex + '", "_type" : "' + dataType + '" } }\n' + jsonData + '\n'

        response = requests.post(esEndpoint + '/_bulk', cert=('access.crt', 'access.key'), verify=False, data=bulkData, headers={'content-type': 'application/json'})
        if (response.status_code != 200 and response.status_code != 201):
            print(response.content)
            print(jsonData)
            raise Exception('Failed writing to ElasticSearch. Status: ' + str(response.status_code))
        #else: Need to check if response has errors, test by changing json dump above to records instead of record
            #jsonResponse = json.load(response.content.decode("utf-8")f)
            #if jsonResponse.errors and jsonResponse.errors == true:
                #raise Exception('Failed writing to ElasticSearch. Status: ' + json.dumps(jsonResponse.items))
    except Exception as ex:
        print('Failed storing analysis: ' + str(ex))
        raise ex
    return

def processRecord(s3, s3Bucket, s3Path, esEndpoint, esIndex, esRawDataIndex, nlp, nlp_spacy, requestId, sentimentFields, groupingIdFields, data, dataType, discardData, ownerField):
    created = strftime("%a %d %b %Y %H:%M:%S +0000", gmtime())

    #Create sentiment text from all text fields flagged for use
    sentimentFieldsArray = sentimentFields.split(',')
    sentimentText = ''
    for sentimentField in sentimentFieldsArray:
        val = data.get(sentimentField.strip())
        if val is not None and len(val) > 0:
            sentimentText += val
            if re.match('.*[?.!;:,]$', val) is not None:
                sentimentText += ' '
            else:
                sentimentText += '.'

    rawResults = {"requestId": requestId, "data": data, "created": created}

    # Retrieve the value of the field that stores who the owner/originator of the data is, or None if one does not exist
    owner = None
    if ownerField is not None:
        owner = data.get(ownerField)
        rawResults['owner'] = owner

    # Create the grouping ID from fields. Grouping ID is used as a unique identifier for each record of data
    groupingId = ''
    if groupingIdFields is not None:
        groupingIdFieldsArray = groupingIdFields.split(',')
        for groupingIdField in groupingIdFieldsArray:
            groupingVal = data.get(groupingIdField.strip())
            if groupingVal is not None:
                groupingId += groupingVal + '.'

    overrideExistingRecords = False
    if len(groupingId) == 0:
        groupingId = str(uuid.uuid4())
    else:
        groupingId = re.sub('[^0-9a-zA-Z]+', '_', groupingId)
        overrideExistingRecords = True

    # Retrieve the sentiment and entities
    sentiment = sentiment_core_nlp(requestId, groupingId, nlp_spacy, nlp, sentimentText, owner, created)

    # If data type has not been passed in, set it as generic.
    # Data type is used to categorize the data passed in
    if not dataType:
        dataType = "generic"

    if not discardData or not bool(discardData):
        #Store the results
        saveCustomerFeedbackAnalysis(s3, s3Bucket, s3Path, esEndpoint, esIndex, esRawDataIndex, groupingId, sentiment, rawResults, dataType, overrideExistingRecords)

    # Build the return JSON object
    entities = {}
    for record in sentiment:
        for entityObj in record.get('entities'):
            entityName = entityObj['name']
            entity = None
            if not entityName in entities:
                entity = {"category": [], "count": 0, "sentiment": {}}
                entities[entityName] = entity
            else:
                entity = entities.get(entityName)

            entity['count'] += 1
            entityCat = entityObj['category']
            
            if entityCat not in entity['category']:
                entity['category'].append(entityObj['category'])

            recordSentiment = record['sentiment'].lower()
            if recordSentiment not in entity['sentiment']:
                entity['sentiment'][recordSentiment] = 1
            else:
                entity['sentiment'][recordSentiment] += 1

    return entities

# Delete any existing records from Elasticsearch that match the request ID
def deleteExistingRecords(esEndpoint, esIndex, esRawDataIndex, requestId):
    response = requests.post(esEndpoint + '/' + esIndex + '/_delete_by_query', data=json.dumps({"query": {"match": {"requestId": requestId}}}),cert=('access.crt', 'access.key'), verify=False,headers={'content-type': 'application/json'})

    #response = requests.post(esEndpoint + '/' + esRawDataIndex + '/_delete_by_query', data=json.dumps({"query": {"match": {"requestId": requestId}}}),cert=('access.crt', 'access.key'), verify=False,headers={'content-type': 'application/json'})

    return

# Delete any existing records from Elasticsearch that match the grouping ID
def deleteExistingRecordsByGroupId(esEndpoint, esIndex, esRawDataIndex, groupingId):
    response = requests.post(esEndpoint + '/' + esIndex + '/_delete_by_query', data=json.dumps({"query": {"match": {"groupingId": groupingId}}}),cert=('access.crt', 'access.key'), verify=False,headers={'content-type': 'application/json'})

    return

# Converts a dictionary to an array of JSON objects
def convertDictionaryToArray(dictionary):
    array = []
    for key in dictionary:
        item = dictionary[key]
        array.append({"name": key, "category": item['category'], "count": item['count'], "sentiment": item['sentiment']})

    return array

# Save the certificate and key to local disk
def saveCerts(cert, key):
    if cert and not os.path.isfile('access.crt'):
        print('write cert')
        with open('access.crt', 'w') as f:
            f.write(cert)

    if key and not os.path.isfile('access.key'):
        print('write key')
        with open('access.key', 'w') as f:
            f.write(key)

# Main function
def main(args):
    nlpServerEndpoint = args.get('nlpServerEndpoint')
    recordId = args.get('id') 
    fileLocation = args.get('file')
    requestId = args.get('requestId')
    sentimentFields = args.get('sentimentFields')
    groupingIdFields = args.get('groupingIdFields')
    ownerField = args.get('ownerField')
    data = args.get('data')
    discardData = args.get('discardData')
    dataType = args.get('type')
    s3Endpoint = args.get('s3Endpoint')
    s3Bucket = args.get('s3Bucket')
    s3Path = args.get('s3Path')
    s3AccessKey = args.get('s3AccessKey')
    s3SecretKey = args.get('s3SecretKey')
    esEndpoint = args.get('elasticSearchEndpoint')
    esIndex = args.get('elasticSearchIndex')
    esRawDataIndex = args.get('elasticSearchRawDataIndex')
    key = args.get('key')
    cert = args.get('cert')

    try:
        saveCerts(cert, key)

        #Create S3 session for writing manifest file
        session = boto3.Session(
            aws_access_key_id=s3AccessKey,
            aws_secret_access_key=s3SecretKey
        )

        s3 = session.resource('s3',
            endpoint_url=s3Endpoint,
            verify=False)

        nlp_spacy = spacy.load('en')
        nlp = StanfordCoreNLP(nlpServerEndpoint)

        results = {}

        # If a request ID has been passed in, delete all records with that ID
        deleteExistingData = False
        if not requestId:
            requestId = str(uuid.uuid4())
        else:
            deleteExistingData = True

        if discardData is not None:
            if isinstance(discardData, str):
                if discardData.lower() in ['1','true','yes','y', 't']:
                    discardData = True
                else:
                    discardData = False
        else:
            discardData = False

        # Check if this is batch or a single record
        count = 0
        error = 0
        if not fileLocation:
            #Process as single record
            if not data or not sentimentFields or not dataType:
                return {"error": "missing parameter(s): data dataType. Ex. {'data':{'Highlights': 'This is the text to analyze'}, 'sentimentFields': 'Highlights', 'dataType':'trip_report'}"}

            # Delete existing data by request ID if it is already in the system
            if deleteExistingData is not None and deleteExistingData == True:
                deleteExistingRecords(esEndpoint, esIndex, esRawDataIndex, requestId)

            results = processRecord(s3, s3Bucket, s3Path, esEndpoint, esIndex, esRawDataIndex, nlp, nlp_spacy, requestId, sentimentFields, groupingIdFields, data, dataType, discardData, ownerField)
            count += 1
        else:
            #Process a batch
            if not sentimentFields or not dataType:
                return {"error": "missing parameter(s): sentimentFields dataType. Ex. {'file': 'data.csv', 'sentimentFields':'Highlights,Lowlights', 'dataType':'trip_report'}"}

	    # get the object
            response = s3.meta.client.get_object(Bucket=s3Bucket, Key=fileLocation)

            # Delete existing data by request ID if it is already in the system
            if deleteExistingData is not None and deleteExistingData == True:
                deleteExistingRecords(esEndpoint, esIndex, esRawDataIndex, requestId)

	    # now iterate over those lines
            for row in csv.DictReader(codecs.getreader('utf-8')(response['Body'])):
                try:
                    resultRecord = processRecord(s3, s3Bucket, s3Path, esEndpoint, esIndex, esRawDataIndex, nlp, nlp_spacy, requestId, sentimentFields, groupingIdFields, row, dataType, discardData, ownerField)
                    for key in resultRecord:
                        if not key in results:
                            results[key] = resultRecord[key]
                        else:
                            record = results[key]
                            returnedRecord = resultRecord[key]
                            record['count'] += returnedRecord['count']
                            record['category'] = list(set(record['category'] + returnedRecord['category']))
                            for sentimentKey in returnedRecord['sentiment']:
                                if sentimentKey not in record['sentiment']:
                                    record['sentiment'][sentimentKey] = returnedRecord['sentiment'][sentimentKey]
                                else:
                                    record['sentiment'][sentimentKey] += returnedRecord['sentiment'][sentimentKey]
                    count += 1
                    #if count == 1:
                    #    break
                except Exception as ex:
                    print('There was a problem processing the record: ' + str(ex))
                    error += 1

        return {"requestId": requestId, "type": dataType, "processed": count, "failed": error, "sentimentFields": sentimentFields, "sentiment": convertDictionaryToArray(results)}

    except Exception as ex:
        return {'error': 'There was a problem processing the sentiment: ' + str(ex)}
